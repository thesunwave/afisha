$(function() {

  // afisha films day selector
  $('ul.nav.second > li').hover(function() {
    $(this).toggleClass("active_nav");
  });

  $('#other_date_btn').click(function(e) {
    e.preventDefault();
    $('ul.b-afisha--header__dropdown').toggleClass('visible');
  });

  $('.b-afisha--header__width--films').eq(0).hover(function() {
    $('ul.b-afisha--header__dropdown').addClass('visible');
  });

  $('.b-afisha--header__width--films').eq(0).mouseleave(function() {
    $('ul.b-afisha--header__dropdown').removeClass('visible');
  });


  // afisha slide up

  $('.b-afisha--timetable__button').on('click', function(e) {
    e.preventDefault();
    var _item = $(this).closest('.b-afisha--film__item');
    _item.find('.b-afisha--film__item--timetable__table').addClass('visible-inline')
    _item.next().addClass("visible");
    _item.find('.b-afisha--timetable__button').addClass('invisible');
  })

  $('.b-afisha--timetable__button--arrow__collapse').on('click', function(e) {
    e.preventDefault();
    var _item = $(this).closest('.b-afisha--film__item--timetable__collapse')
    var _item = _item.prev();
    _item.find('.b-afisha--film__item--timetable__table').removeClass('visible-inline');
    _item.next().removeClass("visible");
    _item.find('.b-afisha--timetable__button').removeClass('invisible');
  })

  // slider

  $('.b-afisha--slider_ul').slick({
    centerMode: true,
    centerPadding: '0px',
    slidesToShow: 3,
    dots: false,
    prevArrow: $('#films-prev'),
    nextArrow: $('#films-next')
  });

  $('.slick-slider').on('click', '.slick-slide', function (e) {
  e.stopPropagation();
  var index = $(this).data("slick-index");
  if ($('.slick-slider').slick('slickCurrentSlide') !== index) {
    $('.slick-slider').slick('slickGoTo', index);
  }
});


  // menu resize
  var menu = function() {
    var $secondMenu = $("#secondMenu");
    var $lis = $secondMenu.find("li").not('#square-style');
    var width = 0;
    var secondMenuWidth = $secondMenu.outerWidth();
    for (var i = 0; i < $lis.length; i++) {
      width += $lis.eq(i).outerWidth();
      if (width > secondMenuWidth) {
        $lis.eq(i).hide().addClass('hided');
      } else {
        $lis.eq(i).show().removeClass('hided');
      }
    }
    if ($lis.eq($lis.length - 1).hasClass('hided') && !$(".zArrow").length) {
      $secondMenu.append('<div class="zArrow" style="display:inline-block;"><span class="glyphicon right"></span></div>');
    }
  }

  $(window)
    .on("resize.menu", menu)
    .trigger("resize.menu");

  $(".zArrow").on("click", function() {
    alert(123);
  });

});
